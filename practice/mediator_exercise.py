class Event(list):
    def __call__(self, *args, **kwargs):
        for item in self:
            item(*args, **kwargs)


class Mediator:
    def __init__(self):
        self.events = Event()

    def fire(self, *args):
        self.events(*args)


class Participant:
    def __init__(self, mediator):
        self.value = 0
        self.mediator = mediator
        self.mediator.events.append(self.increase_value)

    def increase_value(self, sender, value):
        if sender is not self:
            self.value += value

    def say(self, value):
        self.mediator.fire(self, value)

    def __str__(self):
        return f'value: {self.value}'


if __name__ == '__main__':
    group = Mediator()

    p1 = Participant(group)
    p2 = Participant(group)
    p3 = Participant(group)

    p1.say(2)
    p2.say(3)

    print(p1, p2, p3)
